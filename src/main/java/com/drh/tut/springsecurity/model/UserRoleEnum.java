package com.drh.tut.springsecurity.model;

import com.google.common.collect.Sets;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@RequiredArgsConstructor
public enum UserRoleEnum {
  STUDENT(Sets.newHashSet()),
  ADMIN(Sets.newHashSet(
      UserPermsEnum.STUDENT_READ,
      UserPermsEnum.STUDENT_WRITE,
      UserPermsEnum.COURSE_READ,
      UserPermsEnum.COURSE_WRITE)),
  ADMINTRAINEE(Sets.newHashSet(
      UserPermsEnum.STUDENT_READ,
      UserPermsEnum.COURSE_READ));

  @Getter
  private final Set<UserPermsEnum> permissions;


  public Set<GrantedAuthority> grantedAuthorities() {
    Set<GrantedAuthority> authorities = permissions.stream()
        .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
        .collect(Collectors.toSet());
    authorities.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
    return authorities;
  }
}
