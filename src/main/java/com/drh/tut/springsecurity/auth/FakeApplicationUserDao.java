package com.drh.tut.springsecurity.auth;

import com.drh.tut.springsecurity.model.UserRoleEnum;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

@Repository("fake")
@RequiredArgsConstructor
public class FakeApplicationUserDao implements ApplicationUserDao {

  private final PasswordEncoder passwordEncoder;

  @Override
  public Optional<User> selectUserByUsername(String username) {
    return getUsers().stream().filter(user -> username.equals(user.getUsername())).findFirst();
  }

  private List<User> getUsers() {
    return Arrays.asList(
        new User("annasmith", passwordEncoder.encode("password"), UserRoleEnum.STUDENT.grantedAuthorities(), true, true, true, true),
        new User("linda", passwordEncoder.encode("password"), UserRoleEnum.ADMIN.grantedAuthorities(), true, true, true, true),
        new User("tom", passwordEncoder.encode("password"), UserRoleEnum.ADMINTRAINEE.grantedAuthorities(), true, true, true, true)
    );
  }
}
