package com.drh.tut.springsecurity.auth;

import java.util.Optional;

public interface ApplicationUserDao {

  public Optional<User> selectUserByUsername(String username);
}
